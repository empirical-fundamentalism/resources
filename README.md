# Empirical Fundamentalism Resources

Resources for learning about Empirical Fundamentalism

## Introduction

Empirical Fundamentalism is a philosophical system. It provides a conceptual architecture for answering the question, 
"How does everything in reality fit together?" Empirical Fundamentalism is broadly in the Anglo-American analytic 
philosophical tradition and supports its traditional alliance with science, but Empirical Fundamentalism rejects 
orthodox philosophical methodology and the linguistic turn that has plagued philosophy for over a century.

This outline is meant to convey the essential content of Empirical Fundamentalism as briefly as possible. It does not 
provide arguments for adopting Empirical Fundamentalism; that advocacy occurs where a deeper discussion is possible. 
See the suggested reading below.

Empirical Fundamentalism was founded in 2010 based on two innovations: (1) its methodology: empirical analysis, and 
(2) its implementation of the distinction between fundamental and derivative reality. These two work in tandem to 
reformulate philosophical questions so that they can be successfully resolved.

### Empirical Analysis

Empirical Fundamentalism’s methodology advises us to start a philosophical inquiry by attending to any topic you like. 
It might be a concept like liberty or yellow, or it might be a question like, "Why do we know more about the past than 
the future?" or "Would anyone notice if everything doubled in size?"

The next step involves identifying empirical phenomena that make one’s chosen target concepts worth having. For 
example, my concept of knowledge is worth having in part because there are reliable regularities where it is 
beneficial to follow the advice of some people but know others, namely the people who know what they are talking 
about. When I want to gain access to the company warehouse, I ask the company’s security officer, "How do I get 
access?" She says, "You need to fill out form 306B," and when I do that, I eventually get access to the warehouse. 
When I ask the marketing intern, "How do I get access?" I hear the response, "I don’t know," and I make no progress 
toward accessing the warehouse. At least one reason for having a notion of knowledge, to a zeroth-order approximation, 
is that some people give better answers than others, the people who we say know the answers.

Such a rough-and-ready understanding of why we have a concept of knowledge is far from constituting an adequate 
understanding of knowledge. So, to make more progress, in the next stage of empirical analysis, we formulate schematic 
experiments that make rigorous those empirical patterns that motivate our concepts. For example, to flesh out the vague 
idea that people know more about the past than the future, formulate an experiment where people are questioned about 
what happened in the past and what will happen in the future. If constructed carefully, there will be empirical data 
produced by the experiments that vindicate our initial suspicion that we know more about the past. Formulating 
xperimental schemas allows us to characterize noteworthy patterns in the results.

In the next stage of empirical analysis, we scientifically explain the results of the experiments. This usually 
involves placing the results in a broader context and fitting them with other concepts that deserve empirical analysis.

In the final stage of empirical analysis, we hone our concepts to optimize the quality of our scientific explanations.

The signature feature of empirical analysis is that we do not need to account for anything beyond such empirical data: 
the individual observations and empirical patterns relevant to the utility of the target concept. In particular, 
unlike orthodox conceptual analysis, we do not need the completed analysis to make explicitly true any (much less, all) 
of widely agreed upon platitudes pertaining to the concept. For example, the final system of knowledge-relevant 
concepts does not need to make true that Gettier cases are not instances of knowledge; the final analysis only needs 
to make it understandable why people categorize Gettier cases as they do.

Empirical analysis, as sketched here, is an entirely new way of conducting philosophy.  There are no published 
examples of empirical analysis prior to 2010.

### Fundamental Reality

The other innovation of Empirical Fundamentalism is the distinction between fundamental and derivative reality.

Rather than thinking of reality in terms of levels or by contending that one entity is more fundamental than another, 
in Empirical Fundamentalism we think of it in binary terms. Reality consists of fundamental reality, derivative 
reality, and nothing else. In the classic metaphor where God created the universe, we should construe it as God 
bringing about  fundamental reality alone. There was no need (or ability) for God to effect derivative reality or 
any relations that bind derivative reality to fundamental reality. Derivative existents are not part of the world’s 
ontology; only fundamental existents are.

Prototypical derivative existents in nature such as macroscopic physical objects depend on fundamental reality in the 
sense that all of their many possible precisifications each can be represented as a type whose instances are a 
mathematical function of fundamental attributes and purely conventional (fundamentally arbitrary) parameters such as 
coordinates, gauges, and choices about how much to coarse-grain microscopic details. We abstract away from the details 
of fundamental reality to get a determinate derivative existent.

Classically, kinetic energy illustrates this abstraction relation by being a derivative quantity that is a mathematical 
function of fundamental quantities like the masses and relative positions of particles through time as well as 
conventional quantities like a choice of reference frame. Typical derivative attributes, entities, events, and 
processes all abstract away from (and thus reduce to) fundamental reality in the same way.

It is difficult to characterize how much this conception of fundamental and derivative reality differs from all 
previous attempts by philosophers to spell out how everything depends on some ultimate existent, though nothing 
precisely like it has been formulated before. This distinction is compatible with all philosophical traditions and 
thus does not, just by itself, make discernible philosophical progress. Yet, it earns its keep when the Empirical 
Fundamentalist adopts a further doctrine that operationalizes our epistemic grasp of fundamental reality: The Rule. 
According to The Rule, by stipulation, our ideal estimate of fundamental reality is that model which best accounts 
for all empirical phenomena. This operationalization of how to evaluate contending hypotheses about fundamental 
reality earns the label ‘empiricism’. It blocks rationalist metaphysical arguments and makes plausible the hypothesis 
that the universe is ultimately just physics. It is the core non-trivial doctrine of Empirical Fundamentalism.

### Applications

The reason Empirical Fundamentalism succeeds so well at solving philosophical problems is that it facilitates a divide 
and conquer strategy for attacking philosophical problems. Whenever a philosophical question arises, say about whether 
some X exists—free will, a soul, a moral prohibition—the Empirical Fundamentalist first asks whether a fundamental 
X exists. That question is answered largely by whether X integrates well with fundamental physics to explain the 
observable motion of matter. For almost all traditional philosophical issues, X turns out not to make a fruitful 
contribution as a fundamental existent and so arguably does not exist fundamentally. One can then ask whether X exists 
derivatively. In Empirical Fundamentalism, it turns out, the difference between derivative existence and non-existence 
is so unimportant that it can usually be settled without much difficulty. It also makes little practical philosophical 
difference precisely how X is understood so long as it is derivative.

## Suggested Reading

Such a brief summary of Empirical Fundamentalism must omit numerous topics where the details make a big difference, so 
interested readers are invited both to explore other publications that unpack Empirical Fundamentalism and 
to contribute to the exploration of the territory opened up by this new philosophical system.

The initial presentation of Empirical Fundamentalism appears in Volume I of Empirical Fundamentalism, published as 
Causation and Its Basis in Fundamental Physics, which explains it in terms of a detailed application to nature. 
Volume II, forthcoming as Fundamental Reality, says more about the general philosophical system and illustrates it 
with several new applications. Other examples of Empirical Fundamentalism have appeared as journal articles and book
chapters.

### Books

- [Causation and Its Basis in Fundamental Physics](https://philpapers.org/rec/KUTCAI) (Free!) Contains Chapter 1, introducing Empirical Fundamentalism; bonus chapters 11-13, which explain and criticize orthodox analysis more thoroughly; and chapter 14, which is an expanded version of chapter 9 from the printed book. All materials were part of the submission accepted by Oxford University Press.
- [Causation and Its Basis in Fundamental Physics](https://oxford.universitypressscholarship.com/view/10.1093/acprof:oso/9780199936205.001.0001/acprof-9780199936205) (Chapters 1-10, printed by Oxford University Press).
- *Fundamental Reality* (in preparation) Book containing over 390 pages of additional clarification of Empirical Fundamentalism.

### Articles

- [Ontology: An Empirical Fundamentalist Approach](https://brill.com/view/book/edcoll/9789004310827/B9789004310827-s004.xml) An application to the topic of ontology.
- [Reductive Identities: An Empirical Fundamentalist Approach](https://philpapers.org/rec/KUTRIA) An application to the topic of reductive identities.
- [The Empirical Content of the Epistemic Asymmetry](https://philpapers.org/rec/KUTTEC) An application to the topic of the epistemic asymmetry, which concerns why we know more about the past than about the future.
- "Computability: An Empirical Fundamentalist Approach" (in preparation) An application to the distinction between computable and uncomputable functions, Turing machines, and related topics.

## Contributing

The best way to help make philosophical progress is to publish an article or book applying Empirical 
Fundamentalism to address some philosophical topic that interests you.

Another way to help humanity is to write explanations of Empirical Fundamentalism or deeper explorations 
of how it can apply to solve philosophical problems.

If you publish any materials concerning Empirical Fundamentalism, whether in a journal, book, or at a repository such 
as [philpapers.org](philpapers.org) or a video, please issue a pull request to get this site to incorporate your work.

If you want to provide any explanatory materials that are too brief or too long for journal publication, you can insert 
your work into this site, again by pull request. The same goes if you wold like to publicize a meeting or discussion 
concerning Empirical Fundamentalism.

If you do not have the time or ability to produce your own philosophical work, you can help by funding others to 
conduct research using Empirical Fundamentalism. Contribute at [Patreon](https://www.patreon.com/empiricalfundamentalism).

You can follow updates of this page at [@EmpiricalFund](https://twitter.com/EmpiricalFund).
